﻿using SampleWebPage.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleWebPage.DataContext
{
    public class PersonDataContext:DbContext
    {
        public PersonDataContext():base("PersonDataContext")
        {
        }
        public DbSet<Person> Persons { get; set; }
    }
}
