﻿using SampleWebPage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleWebPage.DataContext
{
    public class PersonInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<PersonDataContext>
    {
        protected override void Seed(PersonDataContext context)
        {
            var persons = new List<Person>
            {
            new Person{FirstName="Carson",LastName="Alexander",Age=12},
            new Person{FirstName="Meredith",LastName="Alonso",Age=23},
            new Person{FirstName="Arturo",LastName="Anand",Age=19},
            new Person{FirstName="Gytis",LastName="Barzdukas",Age=46},
            new Person{FirstName="Yan",LastName="Li",Age=22},
            new Person{FirstName="Peggy",LastName="Justice",Age=31},
            new Person{FirstName="Laura",LastName="Norman",Age=32},
            new Person{FirstName="Nino",LastName="Olivetto",Age=25}
            };

            persons.ForEach(s => context.Persons.Add(s));
            context.SaveChanges();
        }
    }
}
