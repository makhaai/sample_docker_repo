﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleWebPage.ViewModels
{
    public class PersonViewModel
    {
        [Required(ErrorMessage = "Firstname is required")]
        [Display(Name = "Firstname")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Lastname is required")]
        [Display(Name = "Lastname")]
        public string LastName { get; set; }
        [Range(18, 80)]
        public int Age { get; set; }
    }
}
